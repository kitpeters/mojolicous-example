package Bean::FakeEmployees::DeptEmp;

# A class that stores associations and the dates thereof between departments and employees
#
# SYNOPSIS
# ========
# 
#   for my $de (@{$employee->dept_emp}) {
#       printf qq{Department %s, %s - %s\n}, $de->department_name, $de->from_date->ymd, $de->to_date->ymd;
#   }
#
#   for my $de (@{$department->dept_emp}) {
#       printf qq{Employee %s, %s - %s\n}, $de->employee_name, $de->from_date->ymd, $de->to_date->ymd; 
#   }
#
# VERSION
# =======
# 0.9
#
# DEPENDENCIES
# ============
# L<Rose::DB::Object>
#
# AUTHOR
# ======
# Kit Peters <kit.peters@broadbean.com>
#
# AUTO METHODS
# =======
# 
# Methods generated by L<Rose::DB::Object>
#
# =head2 dept_no
#
# Department number
#
# =head2 emp_no
#
# Employee number
#
# =head2 from_date
#
# Date this employee began working in this department.  Instance of L<DateTime>
#
# =head2 to_date
#
# Date this employee ceased working in this department.  Instance of L<DateTime>.  If the employee is currently 
# working in this department, the date will be '9999-01-01'
#
# =head2 department
#
# Returns an instance of L<Bean::FakeEmployees::Department>.
#
# =head2 employee
#
# Returns an instance of L<Bean::FakeEmployees::Employee>.
#
use strict;

use base qw(Bean::FakeEmployees::DB::Object);

__PACKAGE__->meta->setup(
    table => 'dept_emp',

    columns => [
        dept_no   => { type => 'character', not_null => 1 },
        emp_no    => { type => 'integer',   not_null => 1 },
        from_date => { type => 'date' },
        to_date   => { type => 'date' },
    ],

    primary_key_columns => [ 'emp_no', 'dept_no' ],

    foreign_keys => [
        department => {
            class       => 'Bean::FakeEmployees::Department',
            key_columns => { dept_no => 'dept_no' },
        },

        employee => {
            class       => 'Bean::FakeEmployees::Employee',
            key_columns => { emp_no => 'emp_no' },
        },
    ],
);
__PACKAGE__->meta->make_manager_class('dept_emp');

# Alias to dept_name
sub department_name { # $department_name ()
    return shift->dept_name;
}

# Return the name of the associated department
sub dept_name { # $department_name ()
    my $self = shift;
    return $self->department->dept_name;
}

# Return the associated employee's name
sub employee_name { # $name () 
    return shift->employee->name;
}

# Return the associated employee's birth date 
sub employee_birth_date { # $birthdate ()
    return shift->employee->birth_date;
}

# Return the associated employee's hire date 
sub employee_hire_date { # $hire_date ()
    return shift->employee->hire_date;
}

# Return the associated employee's first name 
sub employee_first_name { # $first_name ()
    return shift->employee->first_name;
}

# Return the associated employee's last name 
sub employee_last_name { # $last_name ()
    return shift->employee->last_name;
}

# Return the associated employee's gender 
sub employee_gender { # $gender ()
    return shift->employee->gender;
}

1;


#################### pod generated by Pod::Autopod - keep this line to make pod updates possible ####################

=head1 NAME

Bean::FakeEmployees::DeptEmp


=head1 SYNOPSIS


  for my $de (@{$employee->dept_emp}) {
      printf qq{Department %s, %s - %s\n}, $de->department_name, $de->from_date->ymd, $de->to_date->ymd;
  }

  for my $de (@{$department->dept_emp}) {
      printf qq{Employee %s, %s - %s\n}, $de->employee_name, $de->from_date->ymd, $de->to_date->ymd; 
  }



=head1 DESCRIPTION

A class that stores associations and the dates thereof between departments and employees



=head1 REQUIRES


=head1 IMPLEMENTS

L<Bean::FakeEmployees::DB::Object> 


=head1 METHODS

=head2 department_name

 my $department_name = $self->department_name();

Alias to dept_name


=head2 dept_name

 my $department_name = $self->dept_name();

Return the name of the associated department


=head2 employee_birth_date

 my $birthdate = $self->employee_birth_date();

Return the associated employee's birth date


=head2 employee_first_name

 my $first_name = $self->employee_first_name();

Return the associated employee's first name


=head2 employee_gender

 my $gender = $self->employee_gender();

Return the associated employee's gender


=head2 employee_hire_date

 my $hire_date = $self->employee_hire_date();

Return the associated employee's hire date


=head2 employee_last_name

 my $last_name = $self->employee_last_name();

Return the associated employee's last name


=head2 employee_name

 my $name = $self->employee_name();

Return the associated employee's name



=head1 DEPENDENCIES

L<Rose::DB::Object>



=head1 VERSION

0.9



=head1 AUTO METHODS


Methods generated by L<Rose::DB::Object>

=head2 dept_no

Department number

=head2 emp_no

Employee number

=head2 from_date

Date this employee began working in this department.  Instance of L<DateTime>

=head2 to_date

Date this employee ceased working in this department.  Instance of L<DateTime>.  If the employee is currently 
working in this department, the date will be '9999-01-01'

=head2 department

Returns an instance of L<Bean::FakeEmployees::Department>.

=head2 employee

Returns an instance of L<Bean::FakeEmployees::Employee>.



=head1 AUTHOR

Kit Peters <kit.peters@broadbean.com>



=cut


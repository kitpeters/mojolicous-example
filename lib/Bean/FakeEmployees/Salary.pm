package Bean::FakeEmployees::Salary;

use strict;

use base qw(Bean::FakeEmployees::DB::Object);

__PACKAGE__->meta->setup(
    table => 'salaries',

    columns => [
        emp_no    => { type => 'integer', not_null => 1 },
        salary    => { type => 'integer', not_null => 1 },
        from_date => { type => 'date',    not_null => 1 },
        to_date   => { type => 'date',    not_null => 1 },
    ],

    primary_key_columns => [ 'emp_no', 'from_date' ],

    'foreign_keys' => [
        'employee' => {
            'class'       => 'Bean::FakeEmployees::Employee',
            'key_columns' => { 'emp_no' => 'emp_no' },
        },
    ],
);
__PACKAGE__->meta->make_manager_class('salaries');

1;

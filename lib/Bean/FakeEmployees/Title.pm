package Bean::FakeEmployees::Title;

use strict;

use base qw(Bean::FakeEmployees::DB::Object);

__PACKAGE__->meta->setup(
    table => 'titles',

    columns => [
        emp_no    => { type => 'integer', not_null => 1 },
        title     => { type => 'varchar', length   => 50, not_null => 1 },
        from_date => { type => 'date',    not_null => 1 },
        to_date => { type => 'date' },
    ],

    primary_key_columns => [ 'emp_no', 'title', 'from_date' ],
    
    'foreign_keys' => [
        'employee' => {
            'class'       => 'Bean::FakeEmployees::Employee',
            'key_columns' => { 'emp_no' => 'emp_no' },
        },
    ],
);

__PACKAGE__->meta->make_manager_class('titles');

1;

use strict;

package Bean::FakeEmployees::Employee;
use DateTime;

=head1 NAME

Bean::FakeEmployees::Employee

=head1 DEPENDENCIES

L<Rose:DB::Object>
L<DateTime>

=head1 VERSION

0.9

=head1 AUTHOR

Kit Peters <kit.peters@broadbean.com>

=head1 SYNOPSIS


  use Bean::FakeEmployees::Employee;
  my $emp = Bean::FakeEmployees::Employee->new('emp_no' => 10001)->load();
  printf qq{%s was hired on %s\n}, $emp->name, $emp->hire_date;

  $emp->first_name('Joe');
  $emp->last_name('Blow');
  $emp->gender('F');
  $emp->save;

=head1 DESCRIPTION

A class to encapsulate a fake employee

=head1 REQUIRES

L<Bean::FakeEmployees::Employee> 

=head1 IMPLEMENTS

L<Bean::FakeEmployees::DB::Object> 

=head1 METHODS

=head2 emp_no

Get/set the employeee's unique identifier. Don't set this without a really good reason.

=head2 birth_date

Get/set the employee's birth date

=head2 first_name

Get/set the employee's first name

=head2 last_name

Get/set the employee's last name

=head2 gender 

Get/set the employee's gender. Must be one of 'M' or 'F'. No, I don't think that's a good idea either.

=head2 hire_date

Get/set the employee's hire date

=cut

use base qw(Bean::FakeEmployees::DB::Object);

my $THE_END_OF_TIME = '9999-01-01';

__PACKAGE__->meta->setup(
    table => 'employees',

    columns => [
        emp_no     => { type => 'serial',  not_null => 1, 'primary_key' => 1 },
        birth_date => { type => 'date',    not_null => 1 },
        first_name => { type => 'varchar', length   => 14, not_null => 1 },
        last_name  => { type => 'varchar', length   => 16, not_null => 1 },
        gender     => { type => 'enum',    check_in => [ 'M', 'F' ], not_null => 1 },
        hire_date  => { type => 'date',    not_null => 1 },
    ],

    'relationships'     => [
        '_departments' => {
            'type'      => 'many to many',
            'map_class' => 'Bean::FakeEmployees::DeptEmp',
        },
        'dept_emp' => {
            'type' => 'one to many',
            'class' => 'Bean::FakeEmployees::DeptEmp',
            'column_map' => { 'emp_no' => 'emp_no' },
        },
        'dept_manager' => {
            'type' => 'one to many',
            'class' => 'Bean::FakeEmployees::DeptManager',
            'column_map' => { 'emp_no' => 'emp_no' },
        },
        'salaries' => {
            'type' => 'one to many',
            'class' => 'Bean::FakeEmployees::Salary',
            'column_map' => { 'emp_no' => 'emp_no' },
        },
        'titles' => {
            'type' => 'one to many',
            'class' => 'Bean::FakeEmployees::Title',
            'column_map' => { 'emp_no' => 'emp_no' },
        },
    ],

    allow_inline_column_values => 1,
);

__PACKAGE__->meta->make_manager_class('employees');

=head2 current_departments

 $self->current_departments();

Retrieve all departments that this employee is currently a part of. Returns instances of <Bean::FakeEmployees::Department>

  for my $department ($employee->current_departments) {
      printf qq{%s since %s\n},  $department->dept_name, $department->from_date->ymd;
  }

=cut

sub current_departments {
    my $self   = shift;
    my $emp_no = $self->emp_no;

    my @departments = map { $_->department } @{ Bean::FakeEmployees::DeptEmp::Manager->get_dept_emp(
            'query' => { 'emp_no' => $emp_no, 'to_date' => { 'le' => 'now()' } }
        )
    };
    return @departments;
}

=head2 departments 

Get / set the departments this employee is associated with. With no arguments, returns a list of 
L<Bean::FakeEmployees::DeptEmp> objects.  With arguments, returns undef on success, throws exception
on error.  Note that to write the changes to the database, you must call $employee->save();

 my $departments = $self->departments;

 $self->departments( { 'dept_no' => 'd004' } ); # sets the employee as a member of department d004 from today
                                                # until 9999-01-01
=cut

sub departments {
    my $self = shift;

    # Make the many-to-many relationship between employeees and departments work nicely.
    # Since the map table (dept_emp) has additional fields, RDBO's default method won't 
    # work.
    if (!$@) {
        return $self->_departments();
    }
    my @records;
    for my $record (@_) {
        if (ref $record ne 'HASH') { # assume it's a department number
            $record = { 'dept_no' => $record };
        }
        $record->{'from_date'} ||= DateTime->today;
        $record->{'to_date'} ||= $THE_END_OF_TIME;
        push @records, $record;
    }
    $self->dept_emp( @records );
    return;
}

=head2 departments_managed 

Get / set the departments this employee manages. With no arguments, returns a list of 
L<Bean::FakeEmployees::DeptManager> objects.  With arguments, returns undef on success, 
throws exception on error.  Note that to write the changes to the database, you must 
call $employee->save();

    my $departments_managed = $self->departments_managed;
    for my $department (@{$departments_managed}) {
        printf qq{Managed department %s from %s - %s}, $department->department_name, $department->from_date, $department->to_date\n};
    }

=cut

sub departments_managed {
    my $self = shift;
    return $self->dept_manager($@);
}

=head2 add_departments 

Add a department that the employee is associated with. Returns undef on success. Throws exception otherwise.

  $self->add_departments( { 'dept_no' => 'd005' }, { 'dept_no' => 'd006' } );

Or, more laconically:

  $self->add_departments( qw/d005 d006 d007/ );

=cut

sub add_departments {
    my $self = shift;

    my @records;
    for my $record (@_) {
        if (ref $record ne 'HASH') { # assume it's a department number
            $record = { 'dept_no' => $record };
        }
        $record->{'from_date'} ||= DateTime->today;
        $record->{'to_date'} ||= $THE_END_OF_TIME;
        push @records, $record;
    }
    $self->add_dept_emp( @records );
    return;
}

=head2 current_titles

 my $titles = $self->current_titles();

Retrieve all titles that this employee currently holds. Returns instances of <Bean::FakeEmployees::Title>.

  for my $title ($employee->current_titles) {
      printf qq{%s since %s\n},  $title->title, $title->from_date->ymd;
  }

=cut

sub current_titles {
    my $self   = shift;
    my $emp_no = $self->emp_no;

    my @titles = @{ Bean::FakeEmployees::Title::Manager->get_titles(
            'query' => { 'emp_no' => $emp_no, 'to_date' => { 'le' => 'now()' } }
        )
    };
    return @titles;
}

=head2 name

 $self->name();

Return this employee's full name

=cut

sub name {
    my $self = shift;
    return join ' ', $self->first_name, $self->last_name;
}

1;

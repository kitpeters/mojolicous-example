#!/usr/bin/perl

# Sources:
# RoseDB Tutorial: http://search.cpan.org/~jsiracusa/Rose-DB-0.777/lib/Rose/DB/Tutorial.pod
#
# This class will be the base of our interaction with the database using RoseDB.  Creating a trivial subclass,
# as recommended by the RoseDB tutorial, ensures that the "significant" amount of class data that RoseDB stores
# is in a private registry, rather than shared with the base RoseDB object.

use strict;
use warnings;
use English qw(-no_match_vars);

my $DATABASE_DRIVER   = q/mysql/;
my $DATABASE_NAME     = q/employees/;
my $DATABASE_HOST     = q/localhost/;
my $DATABASE_USER     = q/employees_user/;
my $DATABASE_PASSWORD = q//;

package Bean::FakeEmployees::DB;
use Rose::DB;
use parent q(Rose::DB);

__PACKAGE__->use_private_registry;

# We have only one data source here, so register it using the default type / domain
__PACKAGE__->register_db(
    'driver'   => $DATABASE_DRIVER,
    'database' => $DATABASE_NAME,
    'host'     => $DATABASE_HOST,,
    'username' => $DATABASE_USER,
    'password' => $DATABASE_PASSWORD,
);



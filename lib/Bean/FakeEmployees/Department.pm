package Bean::FakeEmployees::Department;

use strict;

use base qw(Bean::FakeEmployees::DB::Object);

__PACKAGE__->meta->setup(
    table => 'departments',

    columns => [
        dept_no   => { type => 'character', length => 4,  not_null => 1 },
        dept_name => { type => 'varchar',   length => 40, not_null => 1 },
    ],

    primary_key_columns => ['dept_no'],

    unique_key => ['dept_name'],

    'relationships'     => [
        'employees' => {
            'type'      => 'many to many',
            'map_class' => 'Bean::FakeEmployees::DeptEmp',
        },
    ],
);
__PACKAGE__->meta->make_manager_class('departments');

1;

#!/usr/bin/perl
#
use Mojolicious::Lite;
use Mojo::JSON qw(decode_json encode_json);
use Readonly;
use Data::Dumper;
use English qw(-no_match_vars);
use DBI;
use lib '../lib';
use Bean::FakeEmployees::Employee;

Readonly my @EMPLOYEE_COLS => qw/birth_date first_name last_name gender hire_date/;

Readonly my %API_USERS => ( 'user1' => 'password1', );
my $dbh = '';

#plugin 'basic_auth_plus';

# This helper exists solely because I am lazy and don't want to type $self->app->log->whatever every time.
helper log => sub {
    my $self = shift;
    return $self->app->log;
};

helper decoded_params => sub {
    my $self = shift;

    my %params;
    foreach my $key (@{$self->req->params->names}) {
        my $val = $self->param($key);

        if ( $val =~ /^[\[\{]/ ) {    # assume it's json
            $self->log->debug(qq{Decoding "$val" as JSON});
            eval {
                $val = decode_json($val);
                1;
            } or do {
                $self->log->error(qq{Error decoding "$val" as JSON: $EVAL_ERROR});
                $self->render( 'status' => 400, 'json' => { 'error' => qq{JSON supplied for "$key" is invalid} } );
                return;
            };
        }
        $params{$key} = $val;
    }

    return \%params;
};

helper add_link_headers => sub {
    my $self = shift;
    my $count = shift;

    my $offset = $self->param('offset') || 0;
    my $limit = $self->param('limit') || 20;

    $self->log->debug(Data::Dumper->Dump([$offset, $limit], [qw/offset limit/]));

    if ($limit > 100) {
        $self->log->debug(qq{Decreasing limit from $limit to 100});
        $limit = 100;
    }
   
    my @link_headers;
    if ( $count - $offset > $limit ) {
        my $url = $self->req->url;
        $url->query->merge( 'offset' => ( $offset + $limit ), 'limit' => $limit );
        push @link_headers, qq{<$url>; rel="next"};
        $self->log->debug(qq{Add "next" link header: $url});
    }

    if ( $offset > 0 ) {
        my $url    = $self->req->url;
        my $offset = $offset - $limit;
        if ( $offset < 0 ) {
            $offset = 0;
        }
        $url->query->merge( 'offset' => $offset, 'limit' => $limit );
        push @link_headers, qq{<$url>; rel="prev"};
        $self->log->debug(qq{Add "prev" link header: $url});
    }

    $self->res->headers->link(@link_headers);
    return 1;
};

# This is executed before every route.  In this case, it is used to handle authentication (badly).  
# This is not how you should handle authentication in the real world.
under sub {
    my $self     = shift;
    my $user     = $self->param('user');
    my $password = $self->param('password');

    $self->log->debug(Data::Dumper->Dump([$self->req->params->to_hash],[qw/params/]));
    $self->log->debug('In under');

    no warnings qw/uninitialized/;

    $self->log->debug(qq{Log in with credentials "$user" / "$password"});
    if ( !$API_USERS{$user} || $API_USERS{$user} ne $password ) {
        $self->render( 'json' => { 'error' => 'Invalid username/password' }, 'status' => 401 );
        return;
    }
    for my $key (qw/offset limit/) {
        my $val = $self->param($key);
        if ($val =~ /\D/) {
            $self->render('json' => {'error' => qq{Invalid value for "$key": $val}, 'status' => 400 });
        }
    }

    use warnings qw/uninitialized/;

    return 1;
};

# All of these endpoints are grouped together.  The under() call below prepends '/employees/:id' to each path
# below.
group {
    under '/employees/:id' => sub {
        my $self = shift;
        my $id   = $self->stash('id');
        $self->log->debug(qq{Get employee with ID $id});
        my $e    = Bean::FakeEmployees::Employee->new( 'emp_no' => $id );
        if ( !$e->load( 'speculative' => 1 ) ) {
            $self->render( 'status' => 422, 'json' => { 'error' => 'Invalid employee number' } );
            return;
        }
        $self->stash( 'employee' => $e );

        return 1;
    };

=pod 
@api {get} /employees/:id Request an employee record
@apiName GetEmployee
@apiParam {Number} id Employee number
@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK 
    {
        'emp_no'     : '12345',
        'first_name' : 'Fake',
        'last_name'  : 'Person',
        'gender'     : 'M',
        'hire_date'  : '1970-01-01',
        'birth_date' : '1970-01-01',
    }
=cut

    # Really '/employees/:id'
    # Retrieve a single employee
    get '/' => sub {
        my $self = shift;

        my @fields_to_return;
        if ( $self->param('fields') ) {
            my @fields = split /\s*,\s*/, $self->param('fields');
            for my $field (@fields) {
                if ( grep { $_ eq $field } @EMPLOYEE_COLS ) {
                    push @fields_to_return, $field;
                }
            }
        }
        else {
            @fields_to_return = @EMPLOYEE_COLS;
        }

        my $ret = {};
        my $e   = $self->stash('employee');
        for my $key (@fields_to_return) {
            $ret->{$key} = $e->$key;
        }

        $self->log->debug( Data::Dumper->Dump( [$ret], [qw/employee/] ) );
        $self->render( 'json' => $ret );
    };

=pod
@api {get} /employees/:id/departments Get departments associated with this employee
@apiName GetEmployeeDepartments
@apiParam {Number} id Employee number
@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK 
    [   
        {   'dept_no' => 'd005',
            'dept_name' => 'The Loony Department',
            'from_date' => '1994-01-01',
            'to_date' => '1998-01-01',
        },
        {   'dept_no' => 'd008',
            'dept_name' => 'The Raving Loony Department',
            'from_date' => '1998-01-01',
            'to_date' => '2010-01-01',
        },
        {   'dept_no' => 'd007',
            'dept_name' => 'The James Bond Department',
            'from_date' => '2010-01-01',
            'to_date' => '9999-01-01',
        }
    ]
=cut
    # Really '/employees/:id/departments'
    # Retrieve department records for a given employee
    get '/departments' => sub {
        my $self = shift;

        my @departments;
        my $e = $self->stash('employee');
        for my $dept_emp ( @{ $e->dept_emp } ) {
            my $department = {
                'from_date' => $dept_emp->from_date,
                'to_date'   => $dept_emp->to_date,
                'dept_no'   => $dept_emp->department->dept_no,
                'dept_name' => $dept_emp->department->dept_name
            };
            push @departments, $department;
        }

        $self->render( 'json' => { 'departments' => \@departments } );
    };

=pod
@api {get} /employees/:id/salaries Get salaries for this employee
@apiParam {Number} id Employee number
@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK 
    [   
        {   'salary' => '1.00',
            'from_date' => '1994-01-01',
            'to_date' => '1998-01-01',
        },
        {   'salary' => '1.50',
            'from_date' => '1998-01-01',
            'to_date' => '2010-01-01',
        },
        {   'salary' => '1.75',
            'dept_name' => 'The James Bond Department',
            'from_date' => '2010-01-01',
            'to_date' => '9999-01-01',
        }
    ]
=cut

    # Retrieve salary records for a given employee
    get '/salaries' => sub {
        my $self = shift;
        my $e = $self->stash('employee');
        
        my @salaries;
        for my $salary (@{$e->salaries}) {
            my $record = { map { $_ => $salary->$_ } qw/salary from_date to_date/ };
            push @salaries, $record;
        }
            
        $self->render( 'json' => \@salaries );
    };

=pod
@api {get} /employees/:id/titles Get titles for this employee
@apiParam {Number} id Employee number
@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK 
    [   
        {   'title' => 'Margrave of Wolverhampton',
            'from_date' => '1994-01-01',
            'to_date' => '1998-01-01',
        },
        {   'title' => 'Gordon of Schumway',
            'from_date' => '1998-01-01',
            'to_date' => '2010-01-01',
        },
        {   'title' => 'Soss of Worcestershire',
            'dept_name' => 'The James Bond Department',
            'from_date' => '2010-01-01',
            'to_date' => '9999-01-01',
        }
    ]
=cut
    # Retrieve title records for a given employee
    get '/titles' => sub {
        my $self = shift;
        my $e = $self->stash('employee');
        
        my @titles;
        for my $title (@{$e->titles}) {
            my $record = { map { $_ => $title->$_ } qw/title from_date to_date/ };
            push @titles, $record;
        }
        $self->render( 'json' => \@titles );
    };
};

=pod
@api {get} /employees Get multiple employees
@apiParam {String} [fields="emp_no", "birth_date", "first_name", "last_name", "gender", "hire_date"] Optional list of fields to retrieve. If not specified, all fields will be retrieved.
@apiParam {String} [sort_by="emp_no", "birth_date", "first_name", "last_name", "gender", "hire_date"] 
@apiParam {Number} [limit] Number of records to limit this query to. Defaults to 20. Maximum of 100.
@apiParam {Number} [offset] Begin query at the given record number. 
@apiSuccess {Object[]} employees List of matching employees
@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
    Link: </employees?user=username&password=password&offset=40&limit=20>; rel="next"
    Link: </employees?user=username&password=password&offset=0&limit=20>; rel="prev"
    [
        {   'emp_no' :  124,
            'birth_date' : '1976-04-15',
            'first_name' : 'Jim',
            'last_name'  : 'Blow',
            'gender'     : 'M',
            'hire_date'  : '2015-01-01',
        },
        {   'emp_no' :  125,
            'birth_date' : '1976-04-15',
            'first_name' : 'John',
            'last_name'  : 'Blow',
            'gender'     : 'M',
            'hire_date'  : '2015-01-01',
        }
    ]

@apiExample {curl} Example with a more complex query - pull all hires in a date range:
    curl -g -i https://localhost/employees?user=username&password=password&hire_date={between:['1987-01-01','1989-01-01']}&limit=5
@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
    Link: </employees?user=username&password=password&hire_date={between:['1987-01-01','1989-01-01']}&offset=5&limit=5>; rel="next"
    [
       {
          "hire_date" : "1989-09-12T00:00:00",
          "gender" : "M",
          "first_name" : "Kyoichi",
          "birth_date" : "1955-01-21T00:00:00",
          "last_name" : "Maliniak"
       },
       {
          "first_name" : "Anneke",
          "birth_date" : "1953-04-20T00:00:00",
          "last_name" : "Preusig",
          "hire_date" : "1989-06-02T00:00:00",
          "gender" : "F"
       },
       {
          "first_name" : "Tzvetan",
          "birth_date" : "1957-05-23T00:00:00",
          "last_name" : "Zielinski",
          "hire_date" : "1989-02-10T00:00:00",
          "gender" : "F"
       },
       {
          "birth_date" : "1963-06-01T00:00:00",
          "first_name" : "Duangkaew",
          "last_name" : "Piveteau",
          "hire_date" : "1989-08-24T00:00:00",
          "gender" : "F"
       },
       {
          "first_name" : "Berni",
          "birth_date" : "1956-02-12T00:00:00",
          "last_name" : "Genin",
          "hire_date" : "1987-03-11T00:00:00",
          "gender" : "M"
       },
    ]

=cut 

# Retreive multiple employees
get '/employees' => sub {
    my $self = shift;

    if ( $self->app->can('json') ) {
        $self->log->debug('I can json');
    }
    else {
        $self->log->debug('I no can json');
    }
    my %args;
    for my $key (qw/limit offset/) {
        if ( defined $self->param($key) ) {
            $args{$key} = $self->param($key);
        }
    }
    my $params = $self->decoded_params;

    for my $key (@EMPLOYEE_COLS) {
        push @{$args{'query'}}, $key, $params->{$key} if defined $params->{$key};
    }
    $args{'limit'} = $self->param('limit') || 20;
    $args{'offset'} = $self->param('offset') || 0;
    if ( $args{'limit'} > 100 ) {
        $args{'limit'} = 100;
    }

    $self->log->debug( Data::Dumper->Dump( [ \%args ], [qw/args/] ) );

    my $count      = Bean::FakeEmployees::Employee::Manager->get_employees_count(%args);
    $self->add_link_headers($count);
    $self->log->debug(qq{Count: $count});

    my @fields_to_return;
    if ( $self->param('fields') ) {
        my @fields = split /\s*,\s*/, $self->param('fields');
        for my $field (@fields) {
            if ( grep { $_ eq $field } @EMPLOYEE_COLS ) {
                push @fields_to_return, $field;
            }
        }
    }
    else {
        @fields_to_return = @EMPLOYEE_COLS;
    }

    my @employees;
    for my $employee ( @{ Bean::FakeEmployees::Employee::Manager->get_employees(%args) } ) {
        my $record = { map { $_ => $employee->$_ } @fields_to_return };
        push @employees, $record;
    }
    $self->render( 'json' => \@employees );
};

# Create a new employee
post '/employees' => sub {
    my $self = shift;
    my $data = $self->req->json;

    for my $key (qw/birth_date first_name last_name gender hire_date/) {
        if ( !$data->{$key} ) {
            $self->render( 'json' => { 'error' => qq{Missing required field "$key"} }, 'status' => 422 );
            return;
        }
    }

    my $query = 'INSERT INTO employees (birth_date, first_name, last_name, gender, hire_date) VALUES (?,?,?,?,?,?)';
    $dbh->do( $query, {}, @{$data}{qw/birth_date first_name last_name gender hire_date/} );
    my $id = $dbh->last_insert_id( undef, undef, 'employees', 'emp_no' );
    if ($id) {
        $self->res->headers->location(qq{/employees/$id});
    }
    else {
        $self->log->error(q{No ID returned from last_insert_id});
        $self->log->error( Data::Dumper->Dump( [$data], [qw/employee/] ) );
        $self->render( 'json' => { 'error' => q{Internal database error} }, 'status' => 500 );
    }
    return;
};

# Explicitly disallow creating an employee by employee number
post '/employees/:id' => sub {
    my $self = shift;
    $self->render( 'text' => 'Method not allowed', 'status' => 405 );
};

# Update multiple employees.  Expected input:
#
#  { 'employees' : [
#   {   'emp_no' :  123,
#       'birth_date' : '1976-03-15',
#       'first_name' : 'Joe',
#       'last_name'  : 'Blow',
#       'gender'     : 'M',
#       'hire_date'  : '2015-01-01',
#   },
#   {   'emp_no' :  124,
#       'birth_date' : '1976-04-15',
#       'first_name' : 'Jim',
#       'last_name'  : 'Blow',
#       'gender'     : 'M',
#       'hire_date'  : '2015-01-01',
#   }
# ] }
#
# Note that all fields, save 'emp_no', are optional
put '/employees' => sub {
    my $self = shift;
    my $data = $self->param('employees');

    my $count = scalar @{$data};
    $self->db->do('BEGIN');
    for ( 0 .. $#{$data} ) {
        my $employee = $data->[$_];
        if ( !$employee->{'emp_no'} ) {
            $self->render( 'text' => qq{Missing required field 'emp_no' in record $_/$count}, 'status' => '422' );
            return;
        }

        my $emp_no = $employee->{'emp_no'};
        my $insert_record;
        for my $key (@EMPLOYEE_COLS) {
            $insert_record->{$key} = $employee->{$key};
        }

        my $query = 'UPDATE employees SET ';
        $query .= join ', ', map {qq{$_ = ?}} keys %{$insert_record};
        $query .= ' WHERE emp_no=?';

        eval {
            $self->db->do( $query, { 'RaiseError' => 1 }, values %{$insert_record} );
            1;
        } or do {
            $self->db->do('ROLLBACK');
            $self->log->eror(qq{Database error: $EVAL_ERROR. Query: $query.});
            $self->render( Data::Dumper->Dump( [$insert_record], [qw/insert_record/] ) );
            $self->render( 'json' => { 'error' => 'Internal database error' }, 'status' => 500 );
        };
    }
    $self->db->do('COMMIT');
    $self->render( 'json' => { 'employees' => $data } );
};


## Update a single employee
#put '/employees/:id' => sub {
#}

#mysql> describe departments;
#+-----------+-------------+------+-----+---------+-------+
#| Field     | Type        | Null | Key | Default | Extra |
#+-----------+-------------+------+-----+---------+-------+
#| dept_no   | char(4)     | NO   | PRI | NULL    |       |
#| dept_name | varchar(40) | NO   | UNI | NULL    |       |
#+-----------+-------------+------+-----+---------+-------+
#2 rows in set (0.00 sec)

# Retrieve a department record for a given department
get 'departments/:id' => sub {
    my $self  = shift;
    my $id    = $self->stash('id');
    my $query = 'SELECT dept_no, dept_name FROM departments WHERE id=?';
    my $sth   = $self->db->prepare($query);
    $sth->execute($id);
    my $department = $sth->fetchrow_hashref;
    $self->render( 'json' => $department );
};

#mysql> describe dept_manager;
#+-----------+---------+------+-----+---------+-------+
#| Field     | Type    | Null | Key | Default | Extra |
#+-----------+---------+------+-----+---------+-------+
#| dept_no   | char(4) | NO   | PRI | NULL    |       |
#| emp_no    | int(11) | NO   | PRI | NULL    |       |
#| from_date | date    | NO   |     | NULL    |       |
#| to_date   | date    | NO   |     | NULL    |       |
#+-----------+---------+------+-----+---------+-------+
#4 rows in set (0.00 sec)

# Retrieve the manager records for a given department
# get '/departments/:id/managers' => sub {
# }
app->start;

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* In an effort to learn Mojolicious, I thought I'd create a project. And since I'm keen on RESTful APIs, I thought I'd create such a thing using an existing public dataset.
* Version 0.9

### How do I get set up? ###

* To set up, download the repo, set up the database (located under files/employees_db-full-<VERSION>.tar.bz2, and then run script/api.pl under Morbo or Hypnotoad or something.  Eventually I'll fiddle it so that it's set up like the rest of our projects.
* Configuration: In lib/Bean/FakeEmployees/DB.pm, edit the various $DATABASE_* values to suit your setup.  
* Dependencies: Rose::DB, Rose::DB::Object
* How to run tests: There aren't any tests yet.
* Deployment instructions: See above

### Contribution guidelines ###

* Writing tests: Go ahead, write some tests. I should probably do that too.
* Other guidelines: Write pretty code

### Who do I talk to? ###

* Talk to me! kitpeters@broadbean.com or find me on Lync.